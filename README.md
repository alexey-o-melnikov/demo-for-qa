# README #

A service for calculating the area of a triangle by the coordinates of its vertices.

For run execute: 

> mvn clean package

> docker-compose up -d


Задачаи:

- написать тест-кейсы

- экспортировать postman коллекцию с успешными запросами, которые использовались при тестировании

- сделать pull request с результатами в master